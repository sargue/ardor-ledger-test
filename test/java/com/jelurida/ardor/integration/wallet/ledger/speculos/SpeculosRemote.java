package com.jelurida.ardor.integration.wallet.ledger.speculos;

public interface SpeculosRemote {
    void start();

    void close();

    void assertAppReady();

    SpeculosRemote assertScreenTexts(String... texts);

    SpeculosRemote assertDualScreenTexts(String firstLine, String secondLine);

    void assertReadyScreen();

    boolean isBlindSigningRequiredScreen();

    void enableBlindSigning();

    SpeculosRemote pressRight();

    @SuppressWarnings("UnusedReturnValue")
    SpeculosRemote pressBoth();
}
