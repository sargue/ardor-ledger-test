package com.jelurida.ardor.integration.wallet.ledger.speculos;

import nxt.addons.JO;
import nxt.util.Logger;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.util.StringContentProvider;
import org.junit.Assert;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SpeculosRemoteHttp implements SpeculosRemote {

    private static final String SPECULOS_API_URL = "http://localhost:5000";
    private static final String[] READY_SCREEN_TEXTS = {"Application", "is ready"};
    private final HttpClient httpClient = new HttpClient();
    private final LinkedBlockingDeque<JO> events = new LinkedBlockingDeque<>();

    enum Button {left, right, both}

    SpeculosRemoteHttp() {}

    @Override
    public void start() {
        try {
            httpClient.start();
            CountDownLatch countDownLatch = new CountDownLatch(1);
            httpClient.newRequest(SPECULOS_API_URL + "/events?stream=true")
                      .accept("text/event-stream")
                      .onResponseBegin(response -> countDownLatch.countDown())
                      .onResponseContent((response, byteBuffer) -> {
                          String event = StandardCharsets.UTF_8.decode(byteBuffer).toString();
                          Logger.logDebugMessage("Received Speculos event: %s", event.replaceAll("\n", ""));
                          events.add(JO.parse(event.substring(event.indexOf('{'))));
                      })
                      .send(result -> Logger.logDebugMessage("Speculos event stream finished."));
            Assert.assertTrue(countDownLatch.await(3, TimeUnit.SECONDS));
            Logger.logDebugMessage("Speculos event listener started.");
        } catch (Exception e) {
            throw new RuntimeException("Starting Jetty HTTP client on SpeculosRemote constructor", e);
        }
    }

    @Override
    public void close() {
        try {
            httpClient.stop();
        } catch (Exception e) {
            throw new RuntimeException("Stopping Jetty HTTP client on SpeculosRemote close()", e);
        }
    }

    @Override
    public void assertAppReady() {
        if (events.isEmpty()) {
            Assert.assertArrayEquals(READY_SCREEN_TEXTS, getCurrentScreenTexts().toArray());
        } else {
            assertScreenTexts(READY_SCREEN_TEXTS);
        }
    }

    @Override
    public SpeculosRemote assertScreenTexts(String... texts) {
        for (String text : texts) {
            JO event = getEvent();
            Assert.assertEquals(text, event.getString("text"));
        }
        return this;
    }

    @Override
    public SpeculosRemote assertDualScreenTexts(String firstLine, String secondLine) {
        // paging on Speculos is dynamic, we don't know if it will be split into two or when exactly
        String firstScreenFirstLine = getEvent().getString("text");
        if (firstScreenFirstLine.equals(firstLine)) {
            // just a single screen
            assertScreenTexts(secondLine);
        } else {
            // first screen line we expect firstLine + " (1/2)"
            Assert.assertEquals(firstLine + " (1/2)", firstScreenFirstLine);
            // we store the first screen, second line text and move to the next one
            JO part1 = getEvent();
            pressRight();
            // second screen we expect firstLine + " (2/2)"
            assertScreenTexts(firstLine + " (2/2)");
            // we fetch the second screen, second line, concatenate it with the first screen, second line and assert
            JO part2 = getEvent();
            Assert.assertEquals(secondLine, part1.getString("text") + part2.getString("text"));
        }
        pressRight();
        return this;
    }

    private JO getEvent() {
        try {
            JO event = events.poll(10, TimeUnit.SECONDS);
            Assert.assertNotNull("Waiting for event from Speculos", event);
            return event;
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted waiting for event from Speculos", e);
        }
    }

    @Override
    public void assertReadyScreen() {
        assertScreenTexts(READY_SCREEN_TEXTS);
    }

    @Override
    public boolean isBlindSigningRequiredScreen() {
        JO event = getEvent();
        String text = event.getString("text");
        if ("ERROR (1/2)".equals(text)) {
            assertScreenTexts("Blind signing must be").pressRight();
            assertScreenTexts("ERROR (2/2)", " enabled on Settings").pressBoth();
            return true;
        }
        events.addFirst(event);
        return false;
    }

    @Override
    public void enableBlindSigning() {
        assertReadyScreen();
        pressRight();
        assertScreenTexts("Settings").pressBoth();
        assertScreenTexts("Allow blind signing", "Back").pressBoth();
        assertScreenTexts("No", "Yes").pressRight();
        assertScreenTexts("No", "Yes", "Back").pressBoth();
        assertScreenTexts("Allow blind signing", "Back").pressRight();
        assertScreenTexts("Allow blind signing", "Back").pressBoth();
        assertReadyScreen();
    }

    @Override
    public SpeculosRemote pressRight() {
        return sendButtonPressAndRelease(Button.right);
    }

    @Override
    public SpeculosRemote pressBoth() {
        return sendButtonPressAndRelease(Button.both);
    }

    private SpeculosRemote sendButtonPressAndRelease(Button button) {
        httpClient.POST(SPECULOS_API_URL + "/button/" + button.name())
            .content(new StringContentProvider("{\"action\":\"press-and-release\"}"))
            .send(result -> Assert.assertTrue("Failed sending button press to Speculos", result.isSucceeded()));
        return this;
    }

    private List<String> getCurrentScreenTexts() {
        try {
            JO jo = JO.parse(httpClient.GET(SPECULOS_API_URL + "/events?currentscreenonly=true").getContentAsString());
            List<String> texts = new ArrayList<>();
            jo.getArray("events").iterator().forEachRemaining(event -> texts.add(event.getString("text")));
            Logger.logDebugMessage("Current screen texts: %s", texts);
            return texts;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new RuntimeException("Reading current screen texts of Speculos", e);
        }
    }
}
