/*
 * Copyright © 2016-2022 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package com.jelurida.ardor.integration.wallet.ledger.application;

import nxt.Tester;
import nxt.account.Account;
import nxt.account.Token;
import nxt.addons.JO;
import nxt.blockchain.ChildChain;
import nxt.http.assetexchange.AssetExchangeTest;
import nxt.http.callers.BroadcastTransactionCall;
import nxt.http.callers.ExchangeCoinsCall;
import nxt.http.callers.GetAccountAssetsCall;
import nxt.http.callers.GetBalanceCall;
import nxt.http.callers.GetCoinExchangeOrderCall;
import nxt.http.callers.ParseTransactionCall;
import nxt.http.callers.SendMoneyCall;
import nxt.http.callers.TransferAssetCall;
import nxt.util.Convert;
import nxt.util.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;

/**
 * To run these tests, you must have a ledger device connected and an Ardor app installed.
 * The ledger device should be unlocked and the Ardor app should be open on the device before starting the test.
 * The tests will display interactive data on the ledger device and ask for confirmations.
 */
public class ArdorAppBridgeInteractiveTest extends AbstractArdorAppBridgeTest {

    @BeforeClass
    public static void initDevice() {
        app = ArdorAppBridge.getApp();
    }

    @Before
    public void checkDeviceStatus() {
        Assert.assertTrue(app.getLastError(), app instanceof ArdorAppBridge);
    }

    @Test
    public void signTransaction() {
        // Get a new account public key from the ledger and fund it
        byte[] ledgerPublicKey = app.getWalletPublicKeys(PATH_STR_0, false);
        long ledgerAccountId = Account.getId(ledgerPublicKey);
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(400000000).feeNQT(100000000).secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        // Create transaction bytes and sign them on the ledger
        JO sendMoneyResponse2 = SendMoneyCall.create(2).recipient(BOB.getStrId()).amountNQT(200000000).feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        Logger.logInfoMessage(sendMoneyResponse2.toJSONString());
        sendToLedger(sendMoneyResponse2);
        JO getBalanceResponse = GetBalanceCall.create(2).account(ledgerAccountId).callNoError();
        Assert.assertEquals("100000000", getBalanceResponse.getString("balanceNQT"));
    }

    @Test
    public void testArdorCoinExchangeOrderIssue() {
        byte[] ledgerPublicKey = app.getWalletPublicKeys(PATH_STR_0, false);
        long ledgerAccountId = Account.getId(ledgerPublicKey);
        SendMoneyCall.create(1).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(200000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        JO tx = ExchangeCoinsCall.create(1).quantityQNT(400000000).exchange(2).priceNQTPerCoin(50000000)
                                 .feeNQT(50000000).publicKey(ledgerPublicKey).callNoError();
        String fullHash = sendToLedger(tx);

        JO response = GetCoinExchangeOrderCall.create().order(Tester.hexFullHashToStringId(fullHash)).call();
        Assert.assertEquals("200000000", response.getString("exchangeQNT"));
        Assert.assertEquals("400000000", response.getString("quantityQNT"));
        Assert.assertEquals(1, response.getInt("chain"));
        Assert.assertEquals("50000000", response.getString("bidNQTPerCoin"));
        Assert.assertEquals(2, response.getInt("exchange"));
        Assert.assertEquals(ledgerAccountId, response.getLong("account"));
    }

    @Test
    public void testCoinExchangeOrderIssue() {
        byte[] ledgerPublicKey = app.getWalletPublicKeys(PATH_STR_0, false);
        long ledgerAccountId = Account.getId(ledgerPublicKey);
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        // Buy 2 BITS coins at 1,5 IGNIS each. Total 3 IGNIS.
        JO tx = ExchangeCoinsCall.create(2).quantityQNT(200000000).exchange(ChildChain.BITSWIFT.getId())
                                 .priceNQTPerCoin(150000000).feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        String fullHash = sendToLedger(tx);

        JO response = GetCoinExchangeOrderCall.create().order(Tester.hexFullHashToStringId(fullHash)).call();
        Assert.assertEquals("300000000", response.getString("exchangeQNT"));
        Assert.assertEquals("200000000", response.getString("quantityQNT"));
        Assert.assertEquals(2, response.getInt("chain"));
        Assert.assertEquals("150000000", response.getString("bidNQTPerCoin"));
        Assert.assertEquals(ChildChain.BITSWIFT.getId(), response.getInt("exchange"));
        Assert.assertEquals(ledgerAccountId, response.getLong("account"));
    }

    @Test
    public void testAssetTransfer() {
        byte[] ledgerPublicKey = app.getWalletPublicKeys(PATH_STR_0, false);
        long ledgerAccountId = Account.getId(ledgerPublicKey);
        String assetId = AssetExchangeTest.issueAsset(ALICE, "LDGR").getAssetIdString();
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        TransferAssetCall.create(2).secretPhrase(ALICE.getSecretPhrase()).recipient(ledgerAccountId)
                         .asset(assetId).quantityQNT(10000).feeNQT(100000000).callNoError();
        generateBlock();

        JO tx = TransferAssetCall.create(2).recipient(BOB.getId()).asset(assetId).quantityQNT(2000)
                                 .feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        sendToLedger(tx);

        JO response = GetAccountAssetsCall.create().account(ledgerAccountId).asset(assetId).call();
        Assert.assertEquals(10000 - 2000, response.getLong("quantityQNT"));
        response = GetAccountAssetsCall.create().account(BOB.getId()).asset(assetId).call();
        Assert.assertEquals(2000, response.getLong("quantityQNT"));
    }

    private static String sendToLedger(JO tx) {
        String unsignedBytesHex = tx.getString("unsignedTransactionBytes");
        Logger.logInfoMessage("Confirm transaction on ledger device");
        Assert.assertTrue(app.loadWalletTransaction(unsignedBytesHex));
        byte[] signature = app.signWalletTransaction(PATH_STR_0);
        String signatureHex = Convert.toHexString(signature);

        // Insert the signature into the transaction bytes and verify the signature
        int sigPos = 2 * SIGNATURE_POSITION;
        int sigLen = 2 * SIGNATURE_LENGTH;
        String signedBytesHex = unsignedBytesHex.substring(0, sigPos) + signatureHex + unsignedBytesHex.substring(sigPos + sigLen);
        JO response = ParseTransactionCall.create().transactionBytes(signedBytesHex).callNoError();
        Assert.assertTrue(response.getBoolean("verify"));

        // Broadcast the transaction and check that it updated the blockchain state
        JO broadcastTransactionResponse = BroadcastTransactionCall.create().transactionBytes(signedBytesHex).callNoError();
        String fullHash = broadcastTransactionResponse.getString("fullHash");
        Assert.assertNotNull(fullHash);
        generateBlock();
        return fullHash;
    }

    @Test
    public void showAddress() {
        app.showAddress(PATH_STR_0);
        Assert.assertTrue(true);
    }

    @Test
    public void generateToken() {
        int timestamp = Convert.toEpochTime(System.currentTimeMillis());
        String tokenData = "Token Data";
        String tokenStr = app.signToken(PATH_STR_0, timestamp, Convert.toHexString(Convert.toBytes(tokenData)));
        Token token = Token.parseToken(tokenStr, tokenData);
        Assert.assertTrue(token.isValid());
        Assert.assertEquals(token.getTimestamp(), timestamp);
        Assert.assertArrayEquals(token.getPublicKey(), app.getWalletPublicKeys(PATH_STR_0, false));
    }

    @Test
    public void generateTokenFromLargeDataSet() {
        Random r = new Random(1);
        int timestamp = r.nextInt();
        byte[] blob = new byte[40000];
        r.nextBytes(blob);
        String tokenStr = app.signToken(PATH_STR_3, timestamp, Convert.toHexString(blob));
        Token token = Token.parseToken(tokenStr, blob);
        Assert.assertTrue(token.isValid());
        Assert.assertEquals(token.getTimestamp(), timestamp);
        Assert.assertArrayEquals(token.getPublicKey(), app.getWalletPublicKeys(PATH_STR_3, false));
    }
}
