/*
 * Copyright © 2016-2022 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package com.jelurida.ardor.integration.wallet.ledger.application;

import com.jelurida.ardor.integration.wallet.ledger.speculos.SpeculosRemote;
import com.jelurida.ardor.integration.wallet.ledger.speculos.SpeculosRemoteManager;
import nxt.account.Account;
import nxt.account.Token;
import nxt.crypto.KeyDerivation;
import nxt.util.Convert;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * To run these tests, you must have a Speculos emulator running.
 * Set the hostname and port using <code>nxt.ledger.speculosHostname</code> and <code>nxt.ledger.speculosPort</code>.
 */
public class ArdorSpeculosTest extends AbstractArdorAppBridgeTest {

    private static final SpeculosRemote speculosRemote = SpeculosRemoteManager.newRemote();

    @BeforeClass
    public static void initDevice() {
        app = ArdorAppBridge.getApp();
        speculosRemote.start();
    }

    @AfterClass
    public static void afterClass() {
        speculosRemote.close();
    }

    @Before
    public void checkDeviceStatus() {
        Assert.assertTrue(app.getLastError(), app instanceof ArdorAppBridge);
    }

    @Test
    public void showAddress0() throws ExecutionException, InterruptedException, TimeoutException {
        showAddress(PATH_STR_0);
    }

    @Test
    public void showAddress3() throws ExecutionException, InterruptedException, TimeoutException {
        showAddress(PATH_STR_3);
    }

    private void showAddress(String path) throws ExecutionException, InterruptedException, TimeoutException {
        byte[] publicKey = KeyDerivation.deriveMnemonic(path, MNEMONIC).getPublicKey();
        long accountId = Account.getId(publicKey);
        String address = Convert.rsAccount(accountId);

        speculosRemote.assertAppReady();
        ForkJoinTask<?> task = ForkJoinPool.commonPool().submit(() -> app.showAddress(path));
        speculosRemote.assertScreenTexts("Your Address (1/2)", address.substring(0, 16)).pressRight()
                      .assertScreenTexts("Your Address (2/2)", address.substring(16)).pressRight()
                      .assertScreenTexts("Done").pressBoth()
                      .assertReadyScreen();

        task.get(3, TimeUnit.SECONDS);
    }

    @Test
    public void generateToken() throws ExecutionException, InterruptedException, TimeoutException {
        int timestamp = Convert.toEpochTime(System.currentTimeMillis());
        String tokenData = "Token Data";

        speculosRemote.assertAppReady();
        ForkJoinTask<String> task = signToken(PATH_STR_0, timestamp, Convert.toBytes(tokenData));
        speculosRemote.assertScreenTexts("Blind", "Signing").pressRight()
                      .assertScreenTexts("Sign token").pressBoth().assertReadyScreen();

        String tokenStr = task.get(3, TimeUnit.SECONDS);
        Token token = Token.parseToken(tokenStr, tokenData);
        Assert.assertTrue(token.isValid());
        Assert.assertEquals(token.getTimestamp(), timestamp);
        Assert.assertArrayEquals(token.getPublicKey(), app.getWalletPublicKeys(PATH_STR_0, false));
    }

    @Test
    public void generateTokenFromLargeDataSet() throws ExecutionException, InterruptedException, TimeoutException {
        Random r = new Random(1);
        int timestamp = r.nextInt();
        byte[] blob = new byte[40000];
        r.nextBytes(blob);

        speculosRemote.assertAppReady();
        ForkJoinTask<String> task = signToken(PATH_STR_3, timestamp, blob);
        speculosRemote.assertScreenTexts("Blind", "Signing").pressRight()
                      .assertScreenTexts("Sign token").pressBoth().assertReadyScreen();

        String tokenStr = task.get(3, TimeUnit.SECONDS);
        Token token = Token.parseToken(tokenStr, blob);
        Assert.assertTrue(token.isValid());
        Assert.assertEquals(token.getTimestamp(), timestamp);
        Assert.assertArrayEquals(token.getPublicKey(), app.getWalletPublicKeys(PATH_STR_3, false));
    }

    private static ForkJoinTask<String> signToken(String accountPath, int timestamp, byte[] tokenData) {
        String tokenHexData = Convert.toHexString(tokenData);
        ForkJoinTask<String> task = submit(() -> app.signToken(accountPath, timestamp, tokenHexData));
        if (speculosRemote.isBlindSigningRequiredScreen()) {
            speculosRemote.enableBlindSigning();
            task = submit(() -> app.signToken(accountPath, timestamp, tokenHexData));
        }
        return task;
    }

}
