package com.jelurida.ardor.integration.wallet.ledger.application;

import com.jelurida.ardor.integration.wallet.ledger.speculos.SpeculosRemote;
import com.jelurida.ardor.integration.wallet.ledger.speculos.SpeculosRemoteManager;
import nxt.Tester;
import nxt.account.Account;
import nxt.addons.JO;
import nxt.blockchain.ChildChain;
import nxt.http.assetexchange.AssetExchangeTest;
import nxt.http.callers.BroadcastTransactionCall;
import nxt.http.callers.ExchangeCoinsCall;
import nxt.http.callers.GetAccountAssetsCall;
import nxt.http.callers.GetBalanceCall;
import nxt.http.callers.GetCoinExchangeOrderCall;
import nxt.http.callers.ParseTransactionCall;
import nxt.http.callers.SendMoneyCall;
import nxt.http.callers.TransferAssetCall;
import nxt.util.Convert;
import nxt.util.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;

public class SpeculosSignTransactionTest extends AbstractArdorAppBridgeTest {
    private static final SpeculosRemote speculosRemote = SpeculosRemoteManager.newRemote();
    private static byte[] ledgerPublicKey;
    private static long ledgerAccountId;

    @BeforeClass
    public static void initDevice() {
        app = ArdorAppBridge.getApp();
        speculosRemote.start();
        speculosRemote.assertAppReady();

        // Get a new account public key from the ledger
        ledgerPublicKey = app.getWalletPublicKeys(PATH_STR_0, false);
        ledgerAccountId = Account.getId(ledgerPublicKey);
    }

    @AfterClass
    public static void afterClass() {
        speculosRemote.close();
    }

    @Before
    public void checkDeviceStatus() {
        Assert.assertTrue(app.getLastError(), app instanceof ArdorAppBridge);
    }

    @Test
    public void testSendMoneyTransaction() throws Exception {
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        JO sendMoneyTx = SendMoneyCall.create(2).recipient(BOB.getStrId()).amountNQT(200000000).feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        sendTransactionToSpeculos(sendMoneyTx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "IGNIS\nMake Payment")
                          .assertScreenTexts("Amount", "2 IGNIS").pressRight()
                          .assertDualScreenTexts("Recipient", BOB.getRsAccount())
                          .assertScreenTexts("Fees", "1 IGNIS").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );
        Assert.assertEquals(500000000 - 200000000 - 100000000,
                            GetBalanceCall.create(2).account(ledgerAccountId).callNoError().getLong("balanceNQT"));
        Assert.assertEquals(200000000, BOB.getChainBalanceDiff(2));
    }

    @Test
    public void testSendMoneyWithMessageTransaction() throws Exception {
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        JO sendMoneyTx = SendMoneyCall.create(2).recipient(BOB.getStrId()).amountNQT(200000000).feeNQT(100000000)
                                      .message("hello world").publicKey(ledgerPublicKey).callNoError();
        sendTransactionToSpeculos(sendMoneyTx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertScreenTexts("Blind", "Signing").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "IGNIS\nMake Payment")
                          .assertScreenTexts("Amount", "2 IGNIS").pressRight()
                          .assertDualScreenTexts("Recipient", BOB.getRsAccount())
                          .assertScreenTexts("Appendages", "Message").pressRight()
                          .assertScreenTexts("Fees", "1 IGNIS").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );

        Assert.assertEquals(500000000 - 200000000 - 100000000,
                            GetBalanceCall.create(2).account(ledgerAccountId).callNoError().getLong("balanceNQT"));
        Assert.assertEquals(200000000, BOB.getChainBalanceDiff(2));
    }

    @Test
    public void testSendArdorTransaction() throws Exception {
        SendMoneyCall.create(1).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(200000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        JO sendArdrTx = SendMoneyCall.create(1).recipient(BOB.getId()).amountNQT(200000000).feeNQT(100000000)
                                     .publicKey(ledgerPublicKey).callNoError();
        sendTransactionToSpeculos(sendArdrTx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "ARDR\nMake Payment")
                          .assertScreenTexts("Amount", "2 ARDR").pressRight()
                          .assertDualScreenTexts("Recipient", BOB.getRsAccount())
                          .assertScreenTexts("Fees", "1 ARDR").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );
        Assert.assertEquals(500000000 - 200000000 - 100000000,
                            GetBalanceCall.create(1).account(ledgerAccountId).callNoError().getLong("balanceNQT"));
        Assert.assertEquals(200000000, BOB.getFxtBalanceDiff());
    }

    @Test
    public void testArdorCoinExchangeOrderIssue() throws Exception {
        SendMoneyCall.create(1).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(200000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        JO tx = ExchangeCoinsCall.create(1).quantityQNT(400000000).exchange(2).priceNQTPerCoin(50000000)
                                 .feeNQT(50000000).publicKey(ledgerPublicKey).callNoError();
        String fullHash = sendTransactionToSpeculos(tx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "ARDR\nIssue Exchange Order")
                          .assertScreenTexts("Amount", "4 IGNIS").pressRight()
                          .assertScreenTexts("Price per IGNIS", "0.5 ARDR").pressRight()
                          .assertScreenTexts("Fees", "0.5 ARDR").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );

        JO response = GetCoinExchangeOrderCall.create().order(Tester.hexFullHashToStringId(fullHash)).call();
        Assert.assertEquals("200000000", response.getString("askNQTPerCoin"));
        Assert.assertEquals("200000000", response.getString("exchangeQNT"));
        Assert.assertEquals("400000000", response.getString("quantityQNT"));
        Assert.assertEquals(1, response.getInt("chain"));
        Assert.assertEquals("50000000", response.getString("bidNQTPerCoin"));
        Assert.assertEquals(2, response.getInt("exchange"));
        Assert.assertEquals(ledgerAccountId, response.getLong("account"));
    }

    @Test
    public void testCoinExchangeOrderIssue() throws Exception {
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        generateBlock();

        // Buy 2 BITS coins at 1,5 IGNIS each. Total 3 IGNIS.
        JO tx = ExchangeCoinsCall.create(2).quantityQNT(200000000).exchange(ChildChain.BITSWIFT.getId())
                                 .priceNQTPerCoin(150000000).feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        String fullHash = sendTransactionToSpeculos(tx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "IGNIS\nIssue Exchange Order")
                          .assertScreenTexts("Amount", "2 BITS").pressRight()
                          .assertScreenTexts("Price per BITS", "1.5 IGNIS").pressRight()
                          .assertScreenTexts("Fees", "1 IGNIS").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );

        JO response = GetCoinExchangeOrderCall.create().order(Tester.hexFullHashToStringId(fullHash)).call();
        Assert.assertEquals("300000000", response.getString("exchangeQNT"));
        Assert.assertEquals("200000000", response.getString("quantityQNT"));
        Assert.assertEquals(2, response.getInt("chain"));
        Assert.assertEquals("150000000", response.getString("bidNQTPerCoin"));
        Assert.assertEquals(ChildChain.BITSWIFT.getId(), response.getInt("exchange"));
        Assert.assertEquals(ledgerAccountId, response.getLong("account"));
    }

    @Test
    public void testAssetTransfer() throws Exception {
        String assetId = AssetExchangeTest.issueAsset(ALICE, "LDGR").getAssetIdString();
        SendMoneyCall.create(2).recipient(ledgerAccountId).amountNQT(500000000).feeNQT(100000000)
                     .secretPhrase(ALICE.getSecretPhrase()).callNoError();
        TransferAssetCall.create(2).secretPhrase(ALICE.getSecretPhrase()).recipient(ledgerAccountId)
                         .asset(assetId).quantityQNT(10000).feeNQT(100000000).callNoError();
        generateBlock();

        JO tx = TransferAssetCall.create(2).recipient(BOB.getId()).asset(assetId).quantityQNT(2000)
                                 .feeNQT(100000000).publicKey(ledgerPublicKey).callNoError();
        sendTransactionToSpeculos(tx, () ->
            speculosRemote.assertScreenTexts("Authorize", "transaction").pressRight()
                          .assertDualScreenTexts("Chain&TxnType", "IGNIS\nTransfer Asset")
                          .assertDualScreenTexts("Asset Id", assetId)
                          .assertScreenTexts("Quantity QNT", "2000").pressRight()
                          .assertDualScreenTexts("Recipient", BOB.getRsAccount())
                          .assertScreenTexts("Fees", "1 IGNIS").pressRight()
                          .assertScreenTexts("Accept", "and send").pressBoth()
        );

        JO response = GetAccountAssetsCall.create().account(ledgerAccountId).asset(assetId).call();
        Assert.assertEquals(10000 - 2000, response.getLong("quantityQNT"));
        response = GetAccountAssetsCall.create().account(BOB.getId()).asset(assetId).call();
        Assert.assertEquals(2000, response.getLong("quantityQNT"));
    }

    private String sendTransactionToSpeculos(JO unsignedTransaction, Runnable callback) throws Exception {
        Logger.logInfoMessage("Sending unsigned transaction bytes to Speculos: %s", unsignedTransaction.toJSONString());
        String unsignedBytesHex = unsignedTransaction.getString("unsignedTransactionBytes");

        speculosRemote.assertAppReady();
        ForkJoinTask<Boolean> loadWalletTxTask = submit(() -> app.loadWalletTransaction(unsignedBytesHex));
        if (speculosRemote.isBlindSigningRequiredScreen()) {
            speculosRemote.enableBlindSigning();
            loadWalletTxTask = submit(() -> app.loadWalletTransaction(unsignedBytesHex));
        }
        callback.run();

        Assert.assertTrue(loadWalletTxTask.get(3, TimeUnit.SECONDS));
        byte[] signature = app.signWalletTransaction(PATH_STR_0);
        String signatureHex = Convert.toHexString(signature);
        Logger.logDebugMessage("Signature: %s", signatureHex);

        // Insert the signature into the transaction bytes and verify the signature
        int sigPos = 2 * SIGNATURE_POSITION;
        int sigLen = 2 * SIGNATURE_LENGTH;
        String signedBytesHex = unsignedBytesHex.substring(0, sigPos) + signatureHex + unsignedBytesHex.substring(sigPos + sigLen);
        JO response = ParseTransactionCall.create().transactionBytes(signedBytesHex).callNoError();
        Assert.assertTrue(response.getBoolean("verify"));

        JO broadcastTransactionResponse = BroadcastTransactionCall.create().transactionBytes(signedBytesHex).callNoError();
        String fullHash = broadcastTransactionResponse.getString("fullHash");
        Assert.assertNotNull(fullHash);
        generateBlock();
        return fullHash;
    }
}
