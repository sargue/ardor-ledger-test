package com.jelurida.ardor.integration.wallet.ledger.speculos;

/**
 * Dummy Speculos Remote implementation which does nothing. Used when the nxt.enableSpeculosRemote property is false
 * or missing. Doesn't assert anything. Useful to test manually a Ledger device or a different app version.
 */
public class SpeculosRemoteDisabled implements SpeculosRemote {
    @Override
    public void start() {}

    @Override
    public void close() {}

    @Override
    public void assertAppReady() {}

    @Override
    public SpeculosRemote assertScreenTexts(String... texts) {
        return this;
    }

    @Override
    public SpeculosRemote assertDualScreenTexts(String firstLine, String secondLine) {
        return this;
    }

    @Override
    public void assertReadyScreen() {}

    @Override
    public boolean isBlindSigningRequiredScreen() {
        return false;
    }

    @Override
    public void enableBlindSigning() {}

    @Override
    public SpeculosRemote pressRight() {
        return this;
    }

    @Override
    public SpeculosRemote pressBoth() {
        return this;
    }
}
