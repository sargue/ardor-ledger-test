package com.jelurida.ardor.integration.wallet.ledger.speculos;

import nxt.Nxt;
import nxt.util.Logger;

public class SpeculosRemoteManager {
    private static final boolean ENABLE_SPECULOS_REMOTE = Nxt.getBooleanProperty("nxt.enableSpeculosRemote", false);

    public static SpeculosRemote newRemote() {
        if (!ENABLE_SPECULOS_REMOTE) {
            Logger.logInfoMessage("Speculos remote is disabled. Set nxt.enableSpeculosRemote=true to enable.");
            return new SpeculosRemoteDisabled();
        }
        return new SpeculosRemoteHttp();
    }
}
